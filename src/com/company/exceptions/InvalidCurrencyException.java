package com.company.exceptions;

public class InvalidCurrencyException extends Exception {

    public InvalidCurrencyException (){
        super("Currency entered incorrectly");
    }

    public InvalidCurrencyException (String message){
        super(message);
    }
}
